package com.example.demo.controller;

import java.util.List;

import com.example.demo.entities.Equity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.example.demo.entities.Portfolio;
import com.example.demo.services.PortfolioService;

@CrossOrigin
@RestController
@RequestMapping("api/portfolio")
public class PortfolioController {

	@Autowired
	PortfolioService service;

	@GetMapping(value = "/get")
	public List<Portfolio> getAllPortfolios() {
		return service.getAllPortfolios();
	}

	@GetMapping(value = "/get/{user_id}")
	public List<Equity> getPortfolioByUser_Id(@PathVariable("user_id") int user_id) {
		return service.getPortfolio(user_id);
	}

	@GetMapping(value = "/checkequityinportfolio/{user_id}/{ticker_symbol}/{quantity}")
	Boolean checkequityinportfolio(@PathVariable int user_id, @PathVariable String ticker_symbol,
			@PathVariable Double quantity) {
		return service.checkequityinportfolio(user_id, ticker_symbol, quantity);
	}

	@GetMapping(value = "/addtoportfolio/{user_id}/{ticker_symbol}/{quantity}/{avg_price}")
	Boolean addtoportfolio(@PathVariable int user_id, @PathVariable String ticker_symbol, @PathVariable Double quantity,
			@PathVariable Double avg_price) {
		return service.addtoportfolio(user_id, ticker_symbol, quantity, avg_price);
	}

	@GetMapping(value = "/removefromportfolio/{user_id}/{ticker_symbol}/{quantity}/{avg_price}")
	Boolean removefromportfolio(@PathVariable int user_id, @PathVariable String ticker_symbol,
			@PathVariable Double quantity, @PathVariable Double avg_price) {
		return service.removefromportfolio(user_id, ticker_symbol, quantity, avg_price);
	}
}
