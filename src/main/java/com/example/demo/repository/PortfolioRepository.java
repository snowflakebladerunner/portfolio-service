package com.example.demo.repository;

import java.util.List;

import com.example.demo.entities.Equity;
import com.example.demo.entities.Portfolio;

public interface PortfolioRepository {
	public List<Portfolio> getAllPortfolios();

	public List<Equity> getPortfolioByUser_Id(int user_id);

	public Boolean checkequityinportfolio(int user_id, String ticker_symbol, Double quantity);

	public Boolean addtoportfolio(int user_id, String ticker_symbol, Double quantity, Double avg_price);

	public Boolean removefromportfolio(int user_id, String ticker_symbol, Double quantity, Double avg_price);

}
