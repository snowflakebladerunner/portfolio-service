package com.example.demo.repository;

import com.example.demo.entities.Equity;
import com.example.demo.entities.Portfolio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.dao.EmptyResultDataAccessException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class MySQLPortfolioRepository implements PortfolioRepository {

    @Autowired
    JdbcTemplate template;

    @Override
    public List<Portfolio> getAllPortfolios() {
        ArrayList<Portfolio> portfolios = new ArrayList<>();
        String sql = "SELECT DISTINCT user_id FROM portfolio";
        List<Integer> users = template.query(sql, new IntRowMapper());
        for (Integer user : users) {
            portfolios.add(new Portfolio(user, getPortfolioByUser_Id(user)));
        }
        return portfolios;
    }

    @Override
    public List<Equity> getPortfolioByUser_Id(int user_id) {
        String sql = "SELECT ticker_symbol, quantity, avg_price FROM portfolio WHERE user_id=?";
        return template.query(sql, new EquityRowMapper(), user_id);
    }

    @Override
    public Boolean checkequityinportfolio(int user_id, String ticker_symbol, Double quantity) {
        String sql1 = "SELECT quantity FROM portfolio WHERE user_id = ? and ticker_symbol = ?";
        double result = template.queryForObject(sql1, Double.class, user_id, ticker_symbol);
        if (result >= quantity) {
            return true;
        } else
            return false;
    }

    @Override
    public Boolean addtoportfolio(int user_id, String ticker_symbol, Double quantity, Double avg_price) {
        String update_sql = "UPDATE portfolio SET quantity = ?, avg_price = ? WHERE user_id = ? and ticker_symbol = ?";
        String insert_sql = "INSERT INTO portfolio(user_id, ticker_symbol, quantity, avg_price) VALUES(?,?,?,?)";

        String sql1 = "SELECT quantity FROM portfolio WHERE user_id = ? and ticker_symbol = ?";
        String sql2 = "SELECT avg_price FROM portfolio WHERE user_id = ? and ticker_symbol = ?";

        try {
            Double get_quantity = template.queryForObject(sql1, Double.class, user_id, ticker_symbol);
            Double get_avg_price = template.queryForObject(sql2, Double.class, user_id, ticker_symbol);
            template.update(update_sql, (quantity + get_quantity),
                    (((get_quantity * get_avg_price) + (quantity * avg_price)) / (get_quantity + quantity)), user_id,
                    ticker_symbol);
        } catch (EmptyResultDataAccessException e) {
            template.update(insert_sql, user_id, ticker_symbol, quantity, avg_price);
        }
        return true;
    }

    @Override
    public Boolean removefromportfolio(int user_id, String ticker_symbol, Double quantity, Double avg_price) {
        String update_sql = "UPDATE portfolio SET quantity = ?, avg_price = ? WHERE user_id = ? and ticker_symbol = ?";
        String delete_sql = "DELETE FROM portfolio WHERE user_id = ? and ticker_symbol = ?";

        String sql1 = "SELECT quantity FROM portfolio WHERE user_id = ? and ticker_symbol = ?";
        String sql2 = "SELECT avg_price FROM portfolio WHERE user_id = ? and ticker_symbol = ?";

        Double get_quantity = template.queryForObject(sql1, Double.class, user_id, ticker_symbol);
        Double get_avg_price = template.queryForObject(sql2, Double.class, user_id, ticker_symbol);

        if (get_quantity > quantity) {
            template.update(update_sql, (get_quantity - quantity),
                    (((get_quantity * get_avg_price) - (quantity * avg_price)) / (get_quantity - quantity)), user_id,
                    ticker_symbol);
        } else if (get_quantity.equals(quantity)) {
            template.update(delete_sql, user_id, ticker_symbol);
        } else {
            return false;
        }

        return true;
    }
}

class IntRowMapper implements RowMapper<Integer> {

    @Override
    public Integer mapRow(ResultSet rs, int i) throws SQLException {
        return rs.getInt("user_id");
    }
}

class EquityRowMapper implements RowMapper<Equity> {

    @Override
    public Equity mapRow(ResultSet rs, int i) throws SQLException {
        return new Equity(rs.getString("ticker_symbol"), rs.getDouble("quantity"), rs.getDouble("avg_price"));
    }
}
