package com.example.demo.services;

import java.util.List;

import com.example.demo.entities.Equity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Portfolio;
import com.example.demo.repository.PortfolioRepository;

@Service
public class PortfolioService {

	@Autowired
	private PortfolioRepository repository;

	public List<Portfolio> getAllPortfolios() {
		return repository.getAllPortfolios();
	}

	public List<Equity> getPortfolio(int user_id) {
		return repository.getPortfolioByUser_Id(user_id);
	}

	public Boolean checkequityinportfolio(int user_id, String ticker_symbol, Double quantity) {
		return repository.checkequityinportfolio(user_id, ticker_symbol, quantity);
	}

	public Boolean addtoportfolio(int user_id, String ticker_symbol, Double quantity, Double avg_price) {
		return repository.addtoportfolio(user_id, ticker_symbol, quantity, avg_price);
	}

	public Boolean removefromportfolio(int user_id, String ticker_symbol, Double quantity, Double avg_price) {
		return repository.removefromportfolio(user_id, ticker_symbol, quantity, avg_price);
	}
}
