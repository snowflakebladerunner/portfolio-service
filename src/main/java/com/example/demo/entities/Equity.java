package com.example.demo.entities;

public class Equity {
    private String ticker;
    private double quantity;
    private double avg_price;

    public Equity(String ticker, double quantity, double avg_price) {
        this.ticker = ticker;
        this.quantity = quantity;
        this.avg_price = avg_price;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getAvg_price() {
        return avg_price;
    }

    public void setAvg_price(double avg_price) {
        this.avg_price = avg_price;
    }

    @Override
    public String toString() {
        return "ReplaceHere{" +
                "ticker='" + ticker + '\'' +
                ", quantity=" + quantity +
                ", avg_price=" + avg_price +
                '}';
    }
}
