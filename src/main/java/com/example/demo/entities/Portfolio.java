package com.example.demo.entities;


import java.util.ArrayList;
import java.util.List;

public class Portfolio {

	private int user_id;
	List<Equity> equities;

	public Portfolio(int user_id, List<Equity> equities) {
		this.user_id = user_id;
		this.equities = equities;
	}

	public Portfolio() {
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public List<Equity> getEquities() {
		return equities;
	}

	public void setEquities(List<Equity> equities) {
		this.equities = equities;
	}

	@Override
	public String toString() {
		return "Portfolio{" +
				"user_id=" + user_id +
				", equities=" + equities +
				'}';
	}
}
